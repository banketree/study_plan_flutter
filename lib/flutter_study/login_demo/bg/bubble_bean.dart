import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///气泡属性配置
class BobbleBean {
  //位置
  Offset postion;

  //颜色
  Color color;

  //运动的速度
  double speed;

  //角度
  double theta;

  //半径
  double radius;
}