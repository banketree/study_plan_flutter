// ignore_for_file: public_member_api_docs, lines_longer_than_80_chars
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// ignore: prefer_mixin
class CounterModel with ChangeNotifier, DiagnosticableTreeMixin {
  int _count = 0;

  int get count => _count;

  void increment() {
    _count++;
    notifyListeners();
  }

  /// Makes `Counter` readable inside the devtools by listing all of its properties
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IntProperty('count', count));
  }
}

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CounterModel()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Example'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Text('You have pushed the button this many times:'),
            CountText(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        key: const Key('increment_floatingActionButton'),

        /// Calls `context.read` instead of `context.watch` so that it does not rebuild
        /// when [Counter] changes.
        onPressed: () => context.read<CounterModel>().increment(),
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}

class CountText extends StatelessWidget {
  const CountText({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('${context.watch<CounterModel>().count}',
        key: const Key('counterState'),
        style: Theme.of(context).textTheme.headline4);
  }
}

class CountText2 extends StatelessWidget {
  const CountText2({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureProvider<int>(
      create: (context) => Future.value(42),
      child: Text('${context.watch<int>()}',
          key: const Key('counterState'),
          style: Theme.of(context).textTheme.headline4),
    );
  }

//最新用法
//  FutureProvider<int?>(
//  initialValue: null,
//  create: (context) => Future.value(42),
//  child: MyApp(),
//  )
//
//  Widget build(BuildContext context) {
//    // be sure to specify the ? in watch<int?>
//    final value = context.watch<int?>();
//    return Text('$value');
//  }
}

class CountText3 extends StatelessWidget {
  CountText3({Key key}) : super(key: key);

  // 定义 ValueNotifier 对象 _counter
  final ValueNotifier<int> _counter = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<int>(
        valueListenable: _counter,
        builder: (context, value, _) {
          return Provider<int>.value(
            value: value,
            child: CountText2(),
          );
        });
  }
}

class Translations {
  const Translations(this._value);

  final int _value;

  String get title => 'You clicked $_value times';
}

class CountText4 extends StatelessWidget {
  CountText4({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CounterModel()),
        ProxyProvider<CounterModel, Translations>(
          update: (_, counter, __) => Translations(counter.count),
        ),
      ],
      child: CountText3(),
    );
  }
}

class MyClass with DiagnosticableTreeMixin {
  MyClass({this.a, this.b});

  final int a;
  final String b;

  String toString2() {
    return '$runtimeType(a: $a, b: $b)';
  }
}

class CountText5 extends StatelessWidget {
  CountText5({Key key}) : super(key: key);

  int _count = 2;

  @override
  Widget build(BuildContext context) {
    return Provider.value(
      value: _count,
      child: Provider.value(
        value: this,
        child: ListBody(
          children: [
            Text("Test $_count"),
            Text("Test ${context.watch<int>().toString()}")
          ],
        ),
      ),
    );
  }
}

class Example extends ChangeNotifier implements ReassembleHandler {
  @override
  void reassemble() {
    print('Did hot-reload');
  }
}
