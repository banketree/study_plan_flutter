import "package:flutter/material.dart";

import './TextField/index.dart' as TextField;

List<WidgetPoint> widgetPoints = [
  WidgetPoint(
    name: 'TextField',
    routerName: TextField.Demo.routeName,
    buildRouter: (BuildContext context) => TextField.Demo(),
  ),
];
