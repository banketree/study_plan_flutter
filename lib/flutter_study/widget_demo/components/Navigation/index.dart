import "package:flutter/material.dart";

import 'BottomNavigationBar/index.dart' as BottomNavigationBar;
import 'BottomNavigationBarItem/index.dart' as BottomNavigationBarItem;

List<WidgetPoint> widgetPoints = [
  WidgetPoint(
    name: 'BottomNavigationBar',
    routerName: BottomNavigationBar.Demo.routeName,
    buildRouter: (BuildContext context) => BottomNavigationBar.Demo(),
  ),
  WidgetPoint(
    name: 'BottomNavigationBarItem',
    routerName: BottomNavigationBarItem.Demo.routeName,
    buildRouter: (BuildContext context) => BottomNavigationBarItem.Demo(),
  )
];
