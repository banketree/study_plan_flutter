import "package:flutter/material.dart";

import 'ListBody/index.dart' as ListBody;
import 'ListView/index.dart' as ListView;
import 'AnimatedList/index.dart' as AnimatedList;

List<WidgetPoint> widgetPoints = [
  WidgetPoint(
    name: 'ListBody',
    routerName: ListBody.Demo.routeName,
    buildRouter: (BuildContext context) => ListBody.Demo(),
  ),
  WidgetPoint(
    name: 'ListView',
    routerName: ListView.Demo.routeName,
    buildRouter: (BuildContext context) => ListView.Demo(),
  ),
  WidgetPoint(
    name: 'AnimatedList',
    routerName: AnimatedList.Demo.routeName,
    buildRouter: (BuildContext context) => AnimatedList.Demo(),
  )
];
