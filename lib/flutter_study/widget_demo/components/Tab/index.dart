import 'package:flutter/material.dart';

import './Tab/index.dart' as Tab;

List<WidgetPoint> widgetPoints = [
  WidgetPoint(
    name: 'Tab',
    routerName: Tab.Demo.routeName,
    buildRouter: (BuildContext context) => Tab.Demo(),
  ),
];
