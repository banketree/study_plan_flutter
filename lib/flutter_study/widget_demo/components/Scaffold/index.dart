import "package:flutter/material.dart";

import 'Scaffold/index.dart' as Scaffold;
import 'ScaffoldState/index.dart' as ScaffoldState;

List<WidgetPoint> widgetPoints = [
  WidgetPoint(
    name: 'Scaffold',
    routerName: Scaffold.Demo.routeName,
    buildRouter: (BuildContext context) => Scaffold.Demo(),
  ),
  WidgetPoint(
    name: 'ScaffoldState',
    routerName: ScaffoldState.Demo.routeName,
    buildRouter: (BuildContext context) => ScaffoldState.Demo(),
  ),
];
