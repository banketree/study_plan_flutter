import "package:flutter/material.dart";

import '../../../model/widget.dart';
import 'Card/index.dart' as Card;

List<WidgetPoint> widgetPoints = [
  WidgetPoint(
    name: 'Card',
    routerName: Card.Demo.routeName,
    buildRouter: (BuildContext context) => Card.Demo(),
  )
];
