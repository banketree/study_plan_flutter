
import 'package:encrypt/encrypt.dart';


class EncryptUtils {
  //AES加密
  static aesEncrypt(String plainText, String keyStr, String ivStr) {
    try {
      final key = Key.fromUtf8(keyStr);
      final iv = IV.fromUtf8(ivStr);
      final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
      final encrypted = encrypter.encrypt(plainText, iv: iv);
      return encrypted.base64;
    } catch (err) {
      print("aes encode error:$err");
      return plainText;
    }
  }

  //AES解密
  static dynamic aesDecrypt(encrypted, String keyStr, String ivStr) {
    try {
      final key = Key.fromUtf8(keyStr);
      final iv = IV.fromUtf8(ivStr);
      final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
      final decrypted = encrypter.decrypt64(encrypted, iv: iv);
      return decrypted;
    } catch (err) {
      print("aes decode error:$err");
      return encrypted;
    }
  }
}