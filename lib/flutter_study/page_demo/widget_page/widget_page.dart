import 'package:flutter/material.dart';

class WidgetPage extends StatefulWidget {
  @override
  SecondPageState createState() => new SecondPageState();
}

class SecondPageState extends State<WidgetPage>
    with AutomaticKeepAliveClientMixin {
  SecondPageState() : super();

  TextEditingController controller;
  String active = 'test';
  String data = '无';

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }

  Widget buildGrid() {
    // 存放最后的widget
    List<Widget> tiles = [];
    Application.widgetTree.children.forEach((dynamic item) {
      tiles.add(new CateCard(category: item));
    });
    return new ListView(
      children: tiles,
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: Theme.of(context).backgroundColor,
      child: this.buildGrid(),
    );
  }
}
