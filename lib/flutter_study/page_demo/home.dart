import 'package:flutter/material.dart';

/// import 'package:flutter/rendering.dart';

import 'package:fluro/fluro.dart';

class AppPage extends StatefulWidget {
  final UserInformation userInfo;

  AppPage(this.userInfo);

  @override
  State<StatefulWidget> createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<AppPage>
    with SingleTickerProviderStateMixin {
  SpUtil sp;
  WidgetControlModel widgetControl = new WidgetControlModel();
  SearchHistoryList searchHistoryList;
  bool isSearch = false;

  List<Widget> _list = List();
  int _currentIndex = 0;
  List tabData = [
    {'text': 'WIDGET', 'icon': Icon(Icons.extension)},
    {'text': '关于手册', 'icon': Icon(Icons.import_contacts)},
    {'text': '个人中心', 'icon': Icon(Icons.account_circle)},
    //https://flutter-go.pub/api/isInfoOpen
  ];
  List<BottomNavigationBarItem> _myTabs = [];
  String appBarTitle;

  @override
  void initState() {
    super.initState();
    print('widget.userInfo    ${widget.userInfo}');
    initSearchHistory();

    if (Application.pageIsOpen == true) {
      // 是否展开业界动态
      tabData.insert(0, {'text': '业界动态', 'icon': Icon(Icons.language)});
      _list
        //..add(FirstPage())
        ..add(MainPage(userInfo: widget.userInfo));
    }
    appBarTitle = tabData[0]['text'];

    for (int i = 0; i < tabData.length; i++) {
      _myTabs.add(BottomNavigationBarItem(
        icon: tabData[i]['icon'],
        title: Text(
          tabData[i]['text'],
        ),
      ));
    }
    _list
      ..add(WidgetPage())
      ..add(FourthPage())
      ..add(UserPage(userInfo: widget.userInfo));
  }

  @override
  void dispose() {
    super.dispose();
  }

  initSearchHistory() async {
    sp = await SpUtil.getInstance();
    String json = sp.getString(SharedPreferencesKeys.searchHistory);
    print("json $json");
    searchHistoryList = SearchHistoryList.fromJSON(json);
  }

  void onWidgetTap(WidgetPoint widgetPoint, BuildContext context) {
    String targetName = widgetPoint.name;
    String targetRouter = widgetPoint.routerName;
    searchHistoryList
        .add(SearchHistory(name: targetName, targetRouter: targetRouter));
    print("searchHistoryList1 ${searchHistoryList.toString()}");
    Application.router.navigateTo(context, targetRouter.toLowerCase(),
        transition: TransitionType.native);
  }

  Widget buildSearchInput(BuildContext context) {
    return new SearchInput((value) async {
      if (value != '') {
        print('value ::: $value');
        // List<WidgetPoint> list = await widgetControl.search(value);
        List<WidgetPoint> list = await DataUtils.searchWidget(value);
        return list
            .map((item) => new MaterialSearchResult<String>(
                  value: item.name,
                  icon: WidgetName2Icon.icons[item.name] ?? null,
                  text: 'widget',
                  onTap: () {
                    onWidgetTap(item, context);
                  },
                ))
            .toList();
      } else {
        return null;
      }
    }, (value) {}, () {});
  }

  renderAppBar(BuildContext context, Widget widget, int index) {
    if (index == 1 && Application.pageIsOpen == true) {
      return AppBar(title: buildSearchInput(context));
    } else if (index == 0 && Application.pageIsOpen == false) {
      return AppBar(title: buildSearchInput(context));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: renderAppBar(context, widget, _currentIndex),
      body: IndexedStack(
        index: _currentIndex,
        children: _list,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _myTabs,
        //高亮  被点击高亮
        currentIndex: _currentIndex,
        //修改 页面
        onTap: _itemTapped,
        //shifting :按钮点击移动效果
        //fixed：固定
        type: BottomNavigationBarType.fixed,

        fixedColor: Theme.of(context).primaryColor,
      ),
    );
  }

  void _itemTapped(int index) {
    setState(() {
      _currentIndex = index;
      appBarTitle = tabData[index]['text'];
    });
  }
}
