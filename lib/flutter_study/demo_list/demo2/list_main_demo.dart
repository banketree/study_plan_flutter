import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() {
  runApp(
    MaterialApp(
      home: ListDemo2Page(),
    ),
  );
}

///代码清单
class ListDemo2Page extends StatefulWidget {
  @override
  _ListDemo2PageState createState() => _ListDemo2PageState();
}

class _ListDemo2PageState extends State<ListDemo2Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("测试Demo"),
      ),
      backgroundColor: Colors.white,
      // Alt + 回车键提示
      //Ctrl + Alt + L 格式化代码
      //Ctrl + E 显示最近编辑的文件列表
      body: ListView.builder(
        //条目个数
        itemCount: 10,
        //每个子条目的样式
        itemBuilder: (BuildContext context, int index) {
          return ListViewDemo2Item(
            index: index,
          );
        },
      ),
    );
  }
}
