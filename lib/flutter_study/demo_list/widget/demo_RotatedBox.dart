import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: DemoRotatedBoxPage(),
  ));
}

///代码清单
class DemoRotatedBoxPage extends StatefulWidget {
  @override
  _DemoRotatedBoxPageState createState() => _DemoRotatedBoxPageState();
}

class _DemoRotatedBoxPageState extends State<DemoRotatedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("RotatedBox 旋转"),
      ),
      backgroundColor: Colors.white,

      ///填充布局
      body: Container(
        padding: EdgeInsets.only(top: 100, left: 100, right: 100),
        child: Row(
          children: [buildTransform()],
        ),
      ),
    );
  }

  buildTransform() {
    return Transform.rotate(
      //旋转的弧度
      angle: pi / 2,
      //旋转的子Widget
      child: Container(
        child: Text(
          'Hello World!',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.blueGrey,
      ),
    );
  }

  buildRotate() {
    return RotatedBox(
      //旋转的倍数
      quarterTurns: 1,
      //旋转的子Widget
      child: Container(
        child: Text(
          'Hello World!',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.blueGrey,
      ),
    );
  }
}
