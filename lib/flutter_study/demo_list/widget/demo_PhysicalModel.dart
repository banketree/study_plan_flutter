import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: DemoPhysicalModel(),
  ));
}

///代码清单
class DemoPhysicalModel extends StatefulWidget {
  @override
  _DemoPhysicalModelState createState() => _DemoPhysicalModelState();
}

class _DemoPhysicalModelState extends State<DemoPhysicalModel> {
  int _currentIndex = 0;
  Color _shadowColor = Colors.deepPurple;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PhysicalModel 自定义阴影"),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: AnimatedPhysicalModel(
          //阴影的圆角
          borderRadius: BorderRadius.all(Radius.circular(10)),
          //阴影形状 默认矩形
          shape: BoxShape.rectangle,
          //背景颜色
          color: Colors.deepPurple,
          //阴影颜色
          shadowColor: _shadowColor,
          //阴影高度 默认为0
          elevation: 20.0,
          //动态切换的时间
          duration: Duration(milliseconds: 600),
          child: Container(
            width: 200,
            height: 200,
            color: Colors.white,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _currentIndex++;
          int flag = _currentIndex % 3;

          if (flag == 0) {
            _shadowColor = Colors.blue;
          } else if (flag == 1) {
            _shadowColor = Colors.red;
          } else {
            _shadowColor = Colors.deepOrange;
          }
          setState(() {});
        },
      ),
    );
  }

  ///[PhysicalModel]的基本使用
  Widget buildPhysicalModel() {
    return PhysicalModel(
      //阴影的圆角
      borderRadius: BorderRadius.all(Radius.circular(10)),
      //阴影形状 默认矩形
      shape: BoxShape.rectangle,
      //背景颜色
      color: Colors.deepPurple,
      //阴影颜色
      shadowColor: Colors.orange,
      //阴影高度 默认为0
      elevation: 20.0,
      child: Container(
        width: 200,
        height: 200,
        color: Colors.white,
      ),
    );
  }
}
