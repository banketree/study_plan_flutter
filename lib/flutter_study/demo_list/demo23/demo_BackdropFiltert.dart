import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() {
  runApp(MaterialApp(
    //不显示 debug标签
    debugShowCheckedModeBanner: false,
    //显示的首页面
    home: DemoBackdropFilterPage(),
  ));
}

///代码清单
class DemoBackdropFilterPage extends StatefulWidget {
  @override
  _DemoBackdropFilterPageState createState() => _DemoBackdropFilterPageState();
}

class _DemoBackdropFilterPageState extends State<DemoBackdropFilterPage> {
  bool _isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 4), () {
      // _isLoading=false;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    //
    return Scaffold(
      appBar: AppBar(
        title: Text("高斯模糊"),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          //居中
          alignment: Alignment.center,
          children: [
            Positioned.fill(
              child: Image.asset(
                "assets/images/sp05.png",
                fit: BoxFit.fitWidth,
              ),
            ),
            Positioned.fill(
              child: buildLoading(),
            ),
          ],
        ),
      ),
    );
  }

  buildLoading() {
    if (!_isLoading) {
      return Container();
    }
    return Stack(
      //居中
      alignment: Alignment.center,
      children: [
        //高斯模糊层
        Positioned.fill(
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 100),
            child: Container(
              color: Color(0x80ffffff),
            ),
          ),
        ),
        LoadingWidget(
          title: "加载中",
        ),
      ],
    );
  }
}
