import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() {
  runApp(MaterialApp(
    //不显示 debug标签
    debugShowCheckedModeBanner: false,
    //显示的首页面
    home: DemoStreamBuilder(),
  ));
}

///代码清单
class DemoStreamBuilder extends StatefulWidget {
  @override
  _DemoStreamBuilderState createState() => _DemoStreamBuilderState();
}

class _DemoStreamBuilderState extends State<DemoStreamBuilder> {
  int _count = 0;

  //流Stream 控制器
  StreamController<int> _streamController = StreamController();

  @override
  void dispose() {
    //销毁
    _streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _count++;
          //发送消息
          _streamController.add(_count);
        },
      ),
      appBar: AppBar(
        title: Text("StreamBuilder"),
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          children: [
            //接收消息
            StreamBuilder<int>(
              //初始值
              initialData: _count,
              //绑定Stream
              stream: _streamController.stream,
              builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                return Text("测试使用 ${snapshot.data}");
              },
            ),

            Text("测试使用"),
            Text("测试使用"),
          ],
        ),
      ),
    );
  }
}
