import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'FlowItemWidget.dart';

class FlowFliterWidget extends StatefulWidget {
  final double flag;

  const FlowFliterWidget({Key key, this.flag = 0.0}) : super(key: key);

  @override
  _FlowFliterWidgetState createState() => _FlowFliterWidgetState();
}

class _FlowFliterWidgetState extends State<FlowFliterWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: FlowItemWidget(),
        ),
        Positioned.fill(
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaY: 0.4, sigmaX: 0.4),
            child: Container(
              color: Colors.grey.withOpacity(widget.flag),
            ),
          ),
        ),
      ],
    );
  }
}
