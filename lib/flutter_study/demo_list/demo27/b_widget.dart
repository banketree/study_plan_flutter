import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ab.dart';

class BWidget extends StatefulWidget {
  @override
  _BWidgetState createState() => _BWidgetState();
}

class _BWidgetState extends State<BWidget> {
  int _result = 1;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //
    return Container(
      color: Colors.blue,
      child: Column(
        children: [
          ElevatedButton(
            onPressed: () {
              _result++;
              //发送数据
              abStreamController.add(_result);
            },
            child: StreamBuilder(
              stream: abStreamController.stream,
              initialData: _result,
              builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                return Text("++ ${snapshot.data}");
              },
            ),
          ),
        ],
      ),
    );
  }
}
