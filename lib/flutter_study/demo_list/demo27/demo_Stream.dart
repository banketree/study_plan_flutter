import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'a_widget.dart';
import 'b_widget.dart';

main() {
  runApp(MaterialApp(
    //不显示 debug标签
    debugShowCheckedModeBanner: false,
    //显示的首页面
    home: DemoStreamPage(),
  ));
}

///代码清单
class DemoStreamPage extends StatefulWidget {
  @override
  _DemoStreamPageState createState() => _DemoStreamPageState();
}

class _DemoStreamPageState extends State<DemoStreamPage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("Stream"),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(10),
        child: Column(children: [
          Container(
            width: 300,
            height: 200,
            child: AWidget(),
          ),
          SizedBox(
            height: 22,
          ),
          Container(
            width: 300,
            height: 200,
            child: BWidget(),
          ),
        ]),
      ),
    );
  }
}
