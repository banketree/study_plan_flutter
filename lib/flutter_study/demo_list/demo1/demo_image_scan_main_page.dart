import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'image_scan_widget.dart';

main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: DemoVideoListPage(),
  ));
}

class DemoVideoListPage extends StatefulWidget {
  @override
  _DemoPiePageState createState() => _DemoPiePageState();
}

class _DemoPiePageState extends State<DemoVideoListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("图片浏览器"),
        centerTitle: true,
      ),
      //添加一个点击事件
      body: InkWell(
        child: Image.asset("assets/images/banner1.png"),
        //点击回调
        onTap: () {
          //定义图片数组
          List<String> imageList = [
            "assets/images/banner1.png",
            "assets/images/banner2.png",
            "assets/images/banner1.png",
            "assets/images/banner2.png",
          ];
          //打开一个新的页页 图片滑动浏览
          NavigatorUtils.pushPageByFade(
            context: context,
            targPage: ImageScanWidget(imageList: imageList),
          );
        },
      ),
    );
  }
}
