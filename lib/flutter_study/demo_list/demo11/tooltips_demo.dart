import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: TipsDemoPage(),
  ));
}

class TipsDemoPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TipsDemoPageState();
  }
}

class _TipsDemoPageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("控件"),
      ),

      ///填充布局
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Tooltip(
              message: "小提示",
              //true 显示在下面，false 显示在上面
              preferBelow: true,
              //显示的时间
              showDuration: Duration(seconds: 3),
              //内边距
              padding: EdgeInsets.only(left: 20, right: 20),
              //垂直偏移
              verticalOffset: 10.0,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.red),
                color: Colors.white,
              ),
              textStyle: TextStyle(color: Colors.blue),
              //外边距
              margin: EdgeInsets.only(top: 10, left: 60),
              child: ElevatedButton(
                onPressed: () {},
                child: Text("想一想"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
