import 'package:flutter/widgets.dart';
import '../flu_intl.dart';

/// 推荐使用IntlUtil获取字符串.
class IntlUtil {
  static String getString(
    BuildContext context,
    String id, {
    String languageCode,
    String countryCode,
    List<Object> params,
    String defValue = '',
  }) {
    return CustomLocalizations.of(context)?.getString(id,
            languageCode: languageCode,
            countryCode: countryCode,
            params: params,
            defValue: defValue) ??
        defValue;
  }
}
