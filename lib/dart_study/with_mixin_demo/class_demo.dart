
class A {
  String getMessage() => 'A';
}

class B {
  String getMessage() => 'B';
}

class P {
  String getMessage() => 'P';
}

class AB extends P with A, B {}

class BA extends P with B, A {}

//总结
//mixin 可以理解为对类的一种“增强”，但它与单继承兼容，因为它的继承关系是线性的。
//with 后面的类会覆盖前面的类的同名方法
//当我们想要在不共享相同类层次结构的多个类之间共享行为时，可以使用 mixin
//当声明一个 mixin 时， on 后面的类是使用 这个mixin 的父类约束。也就是说一个类若是要 with 这个 mixin，则这个类必须继承或实现这个 mixin 的父类约束
void test() {
  String result = '';

  AB ab = AB();
  result += ab.getMessage();

  BA ba = BA();
  result += ba.getMessage();

  print(result);
}
////////////////////////////////////////////////////////////////////////////////

mixin C on A {
  printMessage() {
    super.getMessage();
    print('C');
  }
}

class D with A, B, C {
  printMessage() => super.printMessage();
}

void test2() {
  D().getMessage();
//  C.printMessage(); ???
}
////////////////////////////////////////////////////////////////////////////////
//有时候一个构造函数会调动类中的其他构造函数。
//一个重定向构造函数是没有代码的，在构造函数声明后，使用冒号调用其他构造函数。
//个人理解有一点点像C++里虚基的构造函数，重定向构造函数函数体为空，仅仅是在该类里用来重定向其他的构造函数的.
class Point {
  num x, y;

  Point(this.x, this.y);

  Point.origin(){
    this.x = 10 ;
    this.y = 10 ;
  }

  //构造参数为实例变量直接赋值
  Point.rect(this.x ,this.y);
}

////////////////////////////////////////////////////////////////////////////////


class No {
  void noSuchMethod(Invocation invocation) {
    invocation.namedArguments
        .forEach((k, v) => print("$k $v")); // Symbol("name") hello
    invocation.positionalArguments.forEach(print); // 1 2
    invocation.typeArguments.forEach(print); // num
    print(invocation.memberName); // Symbl("todo1")
  }

  call() {
    print("hello");
  }
}

test3(){
  dynamic no = No();
  no();
}

////////////////////////////////////////////////////////////////////////////////
main(){
  test();
  test2();
  test3();
}