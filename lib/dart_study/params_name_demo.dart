//定义一个带可选参数的方法
String printUserInfo(String username, [int age]) {
  //行参
  return "";
}

//定义一个带默认参数的方法
String printUserInfo2(String username, [String sex = '男', int age]) {
  //行参
  return "";
}

//定义一个命名参数的方法
String printUserInfo3(String username, {String sex = '男', int age}) {
  //行参
  return "";
}

void main() {
  printUserInfo("username");
  printUserInfo2("username", "nv", 22);
  printUserInfo3("username", age: 33);
}