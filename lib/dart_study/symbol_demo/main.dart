import 'dart:core';
import 'dart:mirrors';
import 'foo_lib.dart';


///
main() {
  Symbol lib = new Symbol("foo_lib");
  Symbol clsToSearch = new Symbol("Foo");
  reflectInstanceMethods(lib, clsToSearch);


  // 不透明的动态字符串名称,用于反映库中的元数据
  var varSymbol = Symbol("onlyOne");
  print(varSymbol == #onlyOne);
  print({#onlyOne:"fsdfsdf"});
}

void reflectInstanceMethods(Symbol libraryName, Symbol className) {
  MirrorSystem mirrorSystem = currentMirrorSystem();
  LibraryMirror libMirror = mirrorSystem.findLibrary(libraryName);

  if (libMirror != null) {
    print("Found Library");
    print("checkng...class details..");
    print("No of classes found is : ${libMirror.declarations.length}");
    libMirror.declarations.forEach((s, d) => print(s));

    if (libMirror.declarations.containsKey(className)) print("found class");
    ClassMirror classMirror = libMirror.declarations[className];

    print(
        "No of instance methods found is ${classMirror.instanceMembers.length}");
    classMirror.instanceMembers.forEach((s, v) => print(s));
  }
}

//输出-
//Found Library
//checkng...class details..
//No of classes found is : 1
//Symbol("Foo")
//found class
//No of instance methods found is 8
//Symbol("==")
//Symbol("hashCode")
//Symbol("toString")
//Symbol("noSuchMethod")
//Symbol("runtimeType")
//Symbol("m1")
//Symbol("m2")
//Symbol("m3")