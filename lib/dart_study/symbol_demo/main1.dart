import 'dart:core';
import 'dart:mirrors';
import '../symbol_demo/foo_lib.dart';

///Symbol是一种用于存储人类可读字符串和经过优化以供计算机使用的字符串之间的关系的方法。

void main() {
  Symbol lib = new Symbol("foo_lib");
  //library name stored as Symbol

  Symbol clsToSearch = new Symbol("Foo");
  //class name stored as Symbol

  if (checkIfClassAvailableInlibrary(lib, clsToSearch))
    //searches Foo class in foo_lib library
    print("class found..");
}

bool checkIfClassAvailableInlibrary(Symbol libraryName, Symbol className) {
  MirrorSystem mirrorSystem = currentMirrorSystem();
  LibraryMirror libMirror = mirrorSystem.findLibrary(libraryName);

  if (libMirror != null) {
    print("Found Library");
    print("checkng...class details..");
    print("No of classes found is : ${libMirror.declarations.length}");
    libMirror.declarations.forEach((s, d) => print(s));

    if (libMirror.declarations.containsKey(className)) return true;
    return false;
  }

  return false;
}

//注意，行libMirror.declarations.forEach((s，d)=> print(s)); 将在运行时遍历库中的每个声明，并将声明打印为Symbol类型。

//输出-
//Found Library
//checkng...class details..
//No of classes found is : 1
//Symbol("Foo") //class name displayed as symbol
//class found.
