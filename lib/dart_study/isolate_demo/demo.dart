//同步生成器
//
//使用sync*，返回的是Iterable对象。
//yield会返回moveNext为true,并等待 moveNext 指令。
//调用getSyncGenerator立即返回Iterable对象。
//调用moveNext方法时getSyncGenerator才开始执行。
//异步生成器
//
//使用async*，返回的是Stream对象。
//yield不用暂停，数据以流的方式一次性推送,通过StreamSubscription进行控制。
//调用getAsyncGenerator立即返回Stream,只有执行了listen，函数才会开始执行。
//listen返回一个StreamSubscription 对象进行流监听控制。

import 'dart:async';
import 'dart:io';

import 'dart:isolate';

///可以使用StreamSubscription对象对数据流进行控制。
//同步生成器： 使用sync*，返回的是Iterable对象
Iterable<int> getSyncGenerator(int n) sync* {
  print('start');
  int k = 0;
  while (k < n) {
    yield k++;
  }
  print('end');
}

//异步生成器： 使用async*，返回的是Stream对象
Stream<int> getAsyncGenerator(int n) async* {
  print('start');
  int k = 0;
  while (k < n) {
    //yield不用暂停，数据以流的方式一次性推送,通过StreamSubscription进行控制
    yield k++;
  }
  print('end');
}

test() {
  //同步生成器
  var it = getSyncGenerator(5).iterator;
  while (it.moveNext()) {
    print(it.current);
  }
  print('-----------------------------------------------------');
  //异步生成器
  // ignore: cancel_subscriptions
  StreamSubscription subscription = getAsyncGenerator(5).listen(null);
  subscription.onData((value) {
    print(value);
    if (value >= 2) {
      subscription.pause(); //可以使用StreamSubscription对象对数据流进行控制
    }
  });
}

////////////////////////////////////////////////////////////////////////////////
void foo(var message) {
  print('execution from foo ... the message is :$message');
}

test2() {
  Isolate.spawn(foo, 'Hello!!');
  Isolate.spawn(foo, 'Greetings!!');
  Isolate.spawn(foo, 'Welcome!!');

  print('execution from main1');
  print('execution from main2');
  print('execution from main3');
}

////////////////////////////////////////////////////////////////////////////////
test3() {
  File file = new File(Directory.current.path + "\\data\\contact.txt");
  Future<String> f = file.readAsString();

  //returns a futrue, this is Async method
  f.then((data) => print(data));

  //once file is read , call back method is invoked
  print("End of main");
  //this get printed first, showing fileReading is non blocking or async
}
////////////////////////////////////////////////////////////////////////////////

main() {
  test();
  test2();
  test3();
}
