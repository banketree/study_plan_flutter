import 'dart:isolate';

main() async {
  ReceivePort receivePort = ReceivePort();
  //spawning
  Isolate.spawn(echo, receivePort.sendPort);

  //await-for 循环
  await for (var msg in receivePort) {
    print(msg);
  }

  receivePort.listen((msg) {
    print(msg);
  });
  print('Hello World');
}

echo(SendPort sendPort) async {
  ReceivePort receivePort = ReceivePort();
  sendPort.send("message");
}
