///String数据类型表示字符序列， Dart字符串是UTF 16代码单元的序列，Dart中的字符串值可以使用单引号，双引号或三引号表示。
///单行字符串使用单引号或双引号表示。三引号用于表示多行字符串。
///
///
///
///

test() {
  var temp = "test";

  //返回此字符串的UTF-16代码单元的不可更改列表。
  print(temp.codeUnits);
  //如果此字符串为空，则返回true。
  print(temp.isEmpty);
  //返回字符串的长度，包括空格，制表符和换行符。
  print(temp.length);

  //将此字符串中的所有字符转换为小写。
  print(temp.toLowerCase());
  //将此字符串中的所有字符转换为大写。
  print(temp.toUpperCase());
  //返回没有任何前导和尾随空格的字符串。
  print(temp.trim());
  //将此对象与另一个对象进行比较。
  print(temp.compareTo(""));
  //将所有与指定模式匹配的子字符串替换为给定值。
  print(temp.replaceAll(RegExp(r"a"), ""));
  //在指定分隔符的匹配项处分割字符串，并返回子字符串列表。
  print(temp.split(""));
  //返回此字符串的子字符串，该子字符串从startIndex(包括在内)到endIndex(不包括)之间扩展。
  print(temp.substring(0, 1));
  //返回此对象的字符串表示形式。
  print(temp.toString());
  //返回给定索引的16位UTF-16代码单元。  通过其索引进行访问，返回给定索引的16位UTF-16代码单元。
  print(temp.codeUnitAt(0));
}

test2() {
  // 字符 对应的 编码
  String str1 = "Test";
  print(str1.codeUnitAt(0));
  //Unicode代码点通常表示为\uXXXX ，其中XXXX是4位十六进制值。要指定多于或少于4个十六进制数字，请将值放在大括号中。可以在dart:core库中使用Runes类的构造函数。
  print(str1.runes);

  // 存储单位 数据存储是以“字节”(Byte)为单位
  Runes("🍆 🍉 🍎").forEach((element) {
    print(element);
  });

  //字符串
  var varStr2 = "Test";
  var varStr3 = """
  Test
  """;
  print(str1 == varStr2);
  print(varStr3 == varStr2);
  var varStr4 = r"\n";
  print(varStr4);
  print(str1.compareTo("Test"));
  for (var i in 'e'.allMatches("string Test")) {
    print(i.start);
    print(i.group(0));
  }

  // 字符 匹配
  var varRet1 = 'e'.matchAsPrefix("eee22e2");
  print(varRet1.end);

  // 字符串操作
  print(String.fromCharCodes([0x1D11E]));
  print(String.fromCharCode(0x1D11E));
  print(String.fromEnvironment("name", defaultValue: "Nana"));
  print("Code".endsWith('o'));
  print("Code".startsWith('o'));
  print("Coco".startsWith(new RegExp(r'C'))); // true
  print("Coco".indexOf('C')); // 0
  print("Coco".lastIndexOf('c')); // 2
  print('i love dart'.substring(2, 6)); // love
  print('i love dart'.contains('love')); // true
  print('i love dart'.replaceFirst(RegExp(r"love"), "hate")); // i hate dart
  print('abc abc'
      .replaceFirstMapped(RegExp(r"a"), (v) => v.start.toString())); // 0bc abc
  print('abc abc'.replaceAll(RegExp(r"a"), "aa")); // aabc aabc
  print('i hate dart'.replaceRange(2, 6, "love")); // i love dart
  print('i hate dart'.split(" ")); // ['i','hate', 'dart]
  print("abba".split(RegExp(r"b*"))); // ['a','a'];
  print("ABC".toLowerCase()); // abc
  print("abc".toUpperCase()); // ABC

  print("i hate dart".splitMapJoin(RegExp(r"hate"),
      onMatch: (_) => "love", onNonMatch: (_) => "==")); // ==love==

  print("$str1 hello");
  print("${str1.runes.toString()} hello");
}

test3() {
  var str = String.fromCharCodes([
    13,
    10,
    67,
  ]);
  var i = 0;
  for (var m in RegExp(r"\n(.+)", multiLine: true).allMatches(str)) {
    print(i);
    i++;
    print(m.group(1));
  }
}

main() {
  test();
  test2();
  test3();
}
