/// DefferedLoadException	当延迟的库无法加载时，将引发该错误。
///FromatException	是抛出的异常
///IntegerDivisionByZeroException	当数字除以零时将引发该错误。
///IOEException	它是与输入输出相关的异常的基类。
///IsolateSpawnException	当无法创建隔离时将抛出该错误。
///Timeout	在等待异步结果时发生调度超时时，将引发此错误。
///
///
main() {
  try {} catch (e) {
    print('The marks cannot be negative');
  }
}
