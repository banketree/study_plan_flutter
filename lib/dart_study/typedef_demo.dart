///typedef 或function-type alise (函数类型别名)有助于定义指向内存中可执行代码的指针。简而言之， typedef 可用作引用函数的指针。

typedef TestInt = int Function(String args);
typedef int TestInt2(String args);

int getTest(String args) {
  print(args);
  return 111;
}

test() {
  TestInt testInt = getTest;
  print(testInt(""));
}

///////////////////////////////////////////////////////////////////////////////
typedef ManyOperation(int firstNo, int secondNo); //function signature
Add(int firstNo, int second) {
  print("Add result is ${firstNo + second}");
}

Subtract(int firstNo, int second) {
  print("Subtract result is ${firstNo - second}");
}

Divide(int firstNo, int second) {
  print("Divide result is ${firstNo / second}");
}

Calculator(int a, int b, ManyOperation oper) {
  print("Inside calculator");
  oper(a, b);
}

test2() {
  Calculator(5, 5, Add);
  Calculator(5, 5, Subtract);
  Calculator(5, 5, Divide);
}

///////////////////////////////////////////////////////////////////////////////
// ignore: top_level_function_literal_block
var testFun = (num params) {
  return params * 2;
};

typedef testFuncType = num Function(num params);

testFuncType testFun2 = (num params) => params;

num testFun3(num params) {
  return params;
}

// ignore: missing_return
num testFun5(num params, {num a = 10, num b = 10}) {}
// ignore: missing_return
num testFun6(num params, [num num1 = 20, num num2 = 30]) {}

test3() {
  testFun(2);
  testFun2(3);
  testFun3(5);
  testFun5(1, b: 11);
  testFun6(3, 3, 3);
  [3, 3, 3, 2].map((e) => print(e)).toList();
}

///////////////////////////////////////////////////////////////////////////////
main() {
  //词法闭包称为闭包，是一个函数对象，即使在原始范围内使用该函数，也可以在其词法范围内访问变量。换句话说，它提供了从内部函数访问外部函数范围的权限。让我们了解以下示例。
  void innerMain() {
    print("innerMain");
  }

  innerMain();
  test();
  test2();
  test3();
}
