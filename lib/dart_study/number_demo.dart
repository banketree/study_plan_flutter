///Dart number可以归类为-
///int        -    任意大小的整数。
///double  -    64位(双精度)浮点数，由IEEE 754标准指定， double 数据类型用于表示小数
///

test() {
  var num = 1;
  var numDouble = 1.0;

  //返回数字的hashCode。
  print(num.hashCode);
  //如果数量有限，则为true；否则为false。
  print(num.isFinite);
  //如果数字是正无穷大或负无穷大，则为true，否则为false。
  print(num.isInfinite);
  //如果数字是非数字双精度值，则为true；否则为false。
  print(num.isNaN);
  //如果数字为负，则为true；否则为false。
  print(num.isNegative);
  //根据数字的符号和数值，返回负一，零或加一。
  print(num.sign);
  //如果数字是偶数，则返回true。
  print(num.isEven);
  //如果数字是奇数，则返回true。
  print(num.isOdd);

  //返回数字的绝对值。
  print(num.abs());
  //返回不小于该数字的最小整数。
  print(num.ceil());
  //将此与其他数字进行比较。
  print(num.compareTo(2));
  //返回不大于当前数字的最大整数。
  print(num.floor());
  //将两个数字相除后，返回截断的余数。
  print(num.remainder(3));
  //返回最接近当前数字的整数。
  print(num.round());
  //返回数字的双精度对数。
  print(num.toDouble());
  //返回与数字相等的整数。
  print(num.toInt());
  //返回数字的字符串等效表示形式。
  print(num.toString());
  //丢弃任何小数位后返回整数。
  print(num.truncate());
}

test2() {
  // var 用法
  var varNum = 20;
  int varInt = 20;
  final valTest = 20;

  //const关键字表示编译时间常量，最终变量只能设置一次。我们可以使用final关键字来定义常量。
  // const 用法 他们的对象是同一个
  const compileArray1 = [1, 2, 3];
  const compileArray2 = const [1, 2, 3];
  var compileArray3 = const [1, 2, 3];
  print(compileArray1 == compileArray2);
  print(compileArray3 == compileArray2);

  // num 四舍五入
  num numSome = 20;
  var varRadix = int.parse("99", radix: 20);
  print(varRadix);
  var varRet = int.tryParse("oo");
  print(varRet);
  double d = -2.9;
  print(d.round()); //-3
  print(d.ceil()); //-2
  print(d.floor()); //-3
  print(d.abs()); //2.9
  print(d.truncate()); //-2
  print(d.clamp(29, 30));
  print(d.compareTo(20)); //-1
}

////////////////////////////////////////////////////////////////////////////////
test3() {
  num n1 = null;
  n1 ??= 2;
  print(n1);

  var arr = [1, 2, 3]..add(4)..add(5)..add(6)..add(7);
  print(arr);

  dynamic n2 = 20;
  if (n2 is num) {
    print(n2);
  }

  if (n2 is! String) {
    print(n2);
  }

  num n3 = null;
  print(n3?.toString());
  var n5 = (n3 as String);
  n5 = "Test";
  print(n3);
  print(n5);

  var rule = Rule((x) => true) >= Rule((x) => true);
  rule.checker(19);
}

class Rule {
  Function checker;

  Rule(this.checker);

  Rule operator >=(Rule r2) {
    return Rule((value) {
      if (this.checker(value)) {
        return r2.checker(value);
      }
      return false;
    });
  }
}
////////////////////////////////////////////////////////////////////////////////

main() {
  test();
  test2();
  test3();
}
